from typing import Iterable


def sig_create(hub, iterable: Iterable, **kwargs) -> Iterable:
    """
    Create a progress bar
    """


def sig_update(hub, progress_bar: Iterable, **kwargs):
    """
    Update the progress bar from the creation function
    """
