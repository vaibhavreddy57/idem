esm_test_state:
  test.present:
    - result: True
    - new_state:
        key: value_updated
        name: esm_test_state_updated

#!require:esm_test_state
esm_test_state_ref:
  test.present:
    - result: True
    - new_state:
        key1: ${test:esm_test_state:key}
        name_ref: ${test:esm_test_state:name}
