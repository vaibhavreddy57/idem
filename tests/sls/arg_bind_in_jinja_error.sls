multi_result_new_state:
  test.present:
    - result: True
    - new_state: {"0": {"resource_id": "HZ1", "key_with_nested_value": {"name": "nested_value1"}},
        "1": {"resource_id": "HZ2", "key_with_nested_value": {"name": "nested_value2"}},
        "2": {"resource_id": "HZ3", "key_with_nested_value": {"name": "nested_value3"}}}

#!require:multi_result_new_state

test_arg_bind_in_jinja:
    trigger.build:
        - triggers:
             - resource_ids:  {{ hub.idem.arg_bind.resolve("${test:multi_result_new_state_1}").values() | map(attribute='resource_id') | list }}
