test delay:
  test.succeed_without_changes:
    - require:
      - time: sleep_2s

sleep_2s:
  time.sleep:
    - duration: 2

test delay2:
  test.succeed_without_changes:
    - require:
      - time: invalid delay

invalid delay:
  time.sleep
