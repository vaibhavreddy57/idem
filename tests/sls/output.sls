happy-1:
  test.nop

happy-2:
  test.nop

happy-3:
  test.nop

sad-1:
  test.fail_without_changes

sad-2:
  test.fail_without_changes

ahappy:
  test.anop

present_no_op:
  test.present:
    - result: True
    - old_state:
        hello: world
    - new_state:
        hello: world

present_create_1:
  test.present:
    - changes:
        hello: world1
    - result: True

present_create_2:
  test.present:
    - old_state:
    - changes:
        hello: world2
    - result: True

present_update_1:
  test.present:
    - old_state:
        hello: world
    - changes:
        hello: world1
    - result: True

absent_no_op_1:
  test.absent:
    - result: True
    - old_state:

absent_no_op_2:
  test.absent:
    - result: True
