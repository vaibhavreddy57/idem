# when create_before_destroy is false

recreate_scenario_1:
  test.present:
    - resource_id: idem-test-1
    - new_state:
        key: value
        name: updated-test
        resource_id: idem-test-1
    - result: true
    - recreate_on_update:
        create_before_destroy: false
        prefix:
          key: value
