# when create_before_destroy is true
resource_1:
  test.present:
    - resource_id: idem-test-1
    - new_state:
        key: value
        arg_1: test
        resource_id: idem-test-1
    - result: true
    - recreate_on_update:
        create_before_destroy: true
        prefix:
          key: value

resource_2:
  test.present:
    - resource_id: idem-test-2
    - new_state:
        key: value
        name: test
        arg_1: ${test:resource_1:arg_1}
        resource_id: idem-test-2
    - result: true
