__contracts__ = ["returns", "soft_fail"]


def get(hub, ctx):
    return {"result": True, "comment": None, "ret": ctx}


def more(hub, ctx, *args, **kwargs):
    """
    Return the ctx and all parameters passed to this state.
    """
    return {
        "result": True,
        "comment": None,
        "ret": {"args": args, "kwargs": kwargs, "ctx": ctx},
    }
